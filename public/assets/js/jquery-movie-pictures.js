$.fn.moviePictures = function(options) {
  // Key variables
  let mpSettings = $.extend({
    width: 480,
    height: 300,
    start_position: 2,
    depth: 2,
    depth_spacing: 100,
    depth_spacing_decrease: 5,
    depth_scale_decrease: .1,
    arrows: true,
    frontline: 1,
    frontline_spacing: 10,
    border_radius: 10,
    responsive_delay: 100,
    responsive: [
      {
        query: '(min-width: 610px) and (max-width: 820px)',
        depth: 0,
      },
      {
        query: '(min-width: 560px) and (max-width: 609px)',
        depth: 1,
        depth_spacing: 70,
        width: 280,
        height: 280,
      },
      {
        query: '(max-width: 559px)',
        depth: 0,
        width: 220,
        height: 240,
      }
    ],
  }, options);

  let rSettings = $.extend({}, mpSettings);

  let elRealRoot = this

  let renderMoviePictures = elRoot => {
    elRoot.addClass('movie-pictures')
    
    // Render navigation arrows
    elRoot.append(`<div class="mp-arrows">
      <div class="mp-arrow mp-arrow-left"></div>
      <div class="mp-arrow mp-arrow-right"></div>
    </div>`)

    // Render slides
    let elSlidesWrapper = $('<div class="mp-slides"></div>')
    elRoot.find('[data-mp-slide]').each(function() {
      let elRawSlide = $(this)
      let elSlide = $('<div class="mpslide"></div>')
      let bg = elRawSlide.attr('data-mp-slide')
      if(bg) {
        elSlide.append(`<div class="mpslide-image" style="background-image:url('${bg}')"></div>`)
      }
      elSlide.attr('data-mp-id', elRawSlide.attr('data-mp-id'))
      elRawSlide.removeAttr('data-mp-slide data-mp-id')
      elSlide.appendTo(elSlidesWrapper)
      // If the slide has content
      if($.trim(elRawSlide.html())) {
        let elContent = $('<div class="mpcontent"></div>')
        elContent.append(elRawSlide)
        elContent.appendTo(elSlide)
      }
    })
    elRoot.append(elSlidesWrapper)
  }
  
  this.each(function() {
    let elRoot = $(this)
    renderMoviePictures(elRoot)
    let elSlides = elRoot.find('.mpslide')
    // Key variables
    let mpLength = elSlides.length
    let mpCurrentSlide = rSettings.start_position - 0
    
    /**
     * Move slide to desired position
     * 
     * @param {jQuery} slide 
     * @param {int} slideIndex zero based
     */
    let moveSlide = (slide, slideIndex) => {
      let slideDepth = 0
      let slideZindex = 30
      let slideOpacity = 1
      // Left depth slide
      if(slideIndex < mpCurrentSlide) {
        slideDepth = (slideIndex - mpCurrentSlide) * -1
        slideZindex -= slideDepth
      }
      // Right depth slide
      if(slideIndex >= mpCurrentSlide + rSettings.frontline) {
        slideDepth = slideIndex - (mpCurrentSlide + rSettings.frontline) + 1
        // console.log('right depth slideDepth = ', slideDepth)
        slideZindex -= slideDepth
      }
      // Hidden slide
      if(slideDepth > rSettings.depth) {
        slideDepth = rSettings.depth
        slideZindex = -99
        slideOpacity = 0
      }
      // Define transform and z-index
      // console.log(`moveslide ${slideIndex} / ${slideDepth}`)
      let translates = ['translateX(-50%)']
      // If slide is in the frontline
      if(!slideDepth) {
        if(rSettings.frontline > 1) {
          // odd frontline
          if(rSettings.frontline % 2) {
            let middleIndex = (rSettings.frontline-1) / 2
            middleIndex += mpCurrentSlide
            // If not in the middle
            if(slideIndex !== middleIndex) {
              let signal = slideIndex < middleIndex ? '-' : ''
              let translateHere = 'translateX('+signal+'100%) translateX(' + signal + rSettings.frontline_spacing + 'px) '
              let frontDepth = slideIndex - middleIndex
              if(frontDepth < 0) frontDepth *= -1
              translateHere = translateHere.repeat(frontDepth)
              translates.push(translateHere)
            }
          }
          // even frontline
          else {
            let middleIndex = rSettings.frontline / 2
            middleIndex += mpCurrentSlide
            let signal = slideIndex < middleIndex ? '-' : ''
            let translateHere = 'translateX('+signal+'50%) translateX(' + signal + (rSettings.frontline_spacing / 2) + 'px) '
            let translateHere2 = 'translateX('+signal+'100%) translateX(' + signal + rSettings.frontline_spacing + 'px) '
            let frontDepth = slideIndex < middleIndex ? slideIndex - Math.floor(middleIndex) : Math.floor(middleIndex) - slideIndex;
            if(frontDepth < 0) frontDepth *= -1
            translateHere = translateHere + (frontDepth ? translateHere2.repeat(frontDepth-1) : '')
            console.log(`even #${slideIndex} : ${translateHere} / middleIndex: ${middleIndex} / frontDepth: ${frontDepth}`)
            translates.push(translateHere)
          }
        }
      } else {
        // Base transforms
        if(rSettings.frontline > 1) {
          if(rSettings.frontline % 2) {
            let middleIndex = (rSettings.frontline-1) / 2
            let signal = slideIndex < middleIndex + mpCurrentSlide ? '-' : ''
            let translateHere = 'translateX('+signal+'100%) translateX(' + signal + rSettings.frontline_spacing + 'px) '
            translateHere = translateHere.repeat(middleIndex)
            translates.push(translateHere)
          } else {
            let middleIndex = rSettings.frontline / 2
            middleIndex += mpCurrentSlide
            let signal = slideIndex < middleIndex ? '-' : ''
            let translateHere = 'translateX('+signal+'50%) translateX(' + signal + (rSettings.frontline_spacing / 2) + 'px) '
            let translateHere2 = 'translateX('+signal+'100%) translateX(' + signal + rSettings.frontline_spacing + 'px) '
            // translateHere = translateHere + (middleIndex - 1 ? translateHere2.repeat(middleIndex - 1) : '')
            translates.push(translateHere)
          }
        }
        // Depth transform + scale
        for(let i=0; i<slideDepth; i++) {
          let currentSlideDepth = i + 1
          let translateX = rSettings.depth_spacing - 0
          let scale = 1
          let signal = slideIndex < mpCurrentSlide ? '-' : ''
          scale -= currentSlideDepth * rSettings.depth_scale_decrease
          translateX -= currentSlideDepth * rSettings.depth_spacing_decrease
          // if(position < 0) slideZindex -= 0 - position
          // if(currentSlideDepth >= mpLength - rSettings.depth) slideZindex -= mpLength - currentSlideDepth
          translates.push('translateX(' + signal + translateX + 'px) scale(' + scale + ')')
          // console.log('slide = ', slide, {position, currentSlideDepth, mpLength, depth: rSettings.depth})
        }
      }
      
      if(slideOpacity === 0) slide.hide()
      else slide.show()

      let transform = translates.join(' ')
      slide.css({
        'transform': transform,
        'z-index': slideZindex,
        'opacity': slideOpacity
      })
      
      // console.log(`z-index: ${slideZindex}; transform: ${transform}`)
      // console.groupEnd()
    }

    /**
     * Set a specific slide to frontline
     * 
     * @param {int} slidePosition  index of the slide
     */
    let setCurrentSlide = slidePosition => {
      if(slidePosition < 0 || slidePosition > mpLength - rSettings.frontline) return
      let toSlide = { index: slidePosition, id: elSlides.eq(slidePosition).attr('data-mp-id') }
      let fromSlide = { index: mpCurrentSlide, id: elSlides.eq(mpCurrentSlide).attr('data-mp-id') }
      try {
        elRealRoot.trigger('before_slide_change', [toSlide, fromSlide])
      } catch(e) {}
      mpCurrentSlide = slidePosition
      elSlides.each(function(index) {
        moveSlide($(this), index)
      })
    }

    /**
     * Applies the plugin visual configuration
     */
    let elArrowsWrapper = elRoot.find('.mp-arrows')
    let elBgImages = elSlides.find('.mpslide-image')
    let applyStyles = () => {
      // Responsiveness
      let foundResponsive = false
      for(let rs=0; rs<mpSettings.responsive.length; rs++) {
        let rsp = mpSettings.responsive[rs]
        if(window.matchMedia(rsp.query).matches) {
          rSettings = $.extend({}, mpSettings, rsp)
          foundResponsive = true
          break;
        }
      }
      if(!foundResponsive) rSettings = mpSettings

      // Show arrows
      if(rSettings.arrows) {
        elArrowsWrapper.fadeIn()
      } else {
        elArrowsWrapper.fadeOut()
      }

      // Resize root
      elRoot.css({
        height: rSettings.height
      })

      // Slides css
      elSlides.css({
        width: rSettings.width,
        height: rSettings.height,
      })
      elBgImages.css({
        'border-radius': rSettings.border_radius
      })

      // Update slides position
      setCurrentSlide(mpCurrentSlide)
    }

    applyStyles()

    // Move slides on click them
    elSlides.each(function(index) {
      let hm = new Hammer(this)
      hm.on('tap press panright panleft', function(ev) {
        console.log('chegou aqui ev ', ev)
      })
      $(this).on('click', (e) => {
        // if(index === mpCurrentSlide) return;
        if(index >= mpCurrentSlide && index < mpCurrentSlide + rSettings.frontline) return
        e.stopPropagation()
        e.preventDefault()
        if(index < mpCurrentSlide) {
          setCurrentSlide(index)
        } else {
          setCurrentSlide(index - rSettings.frontline + 1)
        }
      })
    })

    // Move slides on swiping
    if(Hammer) {
      // console.log('Hammer loaded')
      // let hm = new Hammer(elSlides)
      // window.hm = hm
      // hm.on('swipeleft tap press swiperight panleft panright', (ev) => {
      //   console.log('SWIPE LEFT ', ev.type)
      //   setCurrentSlide(mpCurrentSlide-1)
      // })
      // hm.on('swiperight', () => setCurrentSlide(mpCurrentSlide+1))
    }

    // Bind arrows
    elRoot.find('.mp-arrow-left').on('click', () => setCurrentSlide(mpCurrentSlide-1))
    elRoot.find('.mp-arrow-right').on('click', () => setCurrentSlide(mpCurrentSlide+1))

    // Resize responsive
    let resizeTimeout = null
    window.addEventListener('resize', e => {
      clearTimeout(resizeTimeout)
      resizeTimeout = setTimeout(() => {
        applyStyles()
      }, mpSettings.responsive_delay)
    })
  })

  return this
}
